@extends('layout.master')

@section('judul')
    Edit Pemain {{$cast->nama}}
@endsection

@section('content')
    
<div>
    <h2>Edit Pemain</h2>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group"> 
                <label>Nama Pemain Film</label>
                <input type="text" class="form-control" name="nama" id="nama" value="{{$cast->nama}}" placeholder="Masukkan Nama Pemain">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" value="{{$cast->umur}}" placeholder="Masukkan Umur Pemain">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio</label>
                <textarea type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan Umur Pemain">{{$cast->bio}}</textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>
@endsection