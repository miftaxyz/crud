@extends('layout.master')

@section('judul')
    Detail Pemain {{$cast->nama}} 
@endsection

@section('content')
    <h1>Nama: {{$cast->nama}}</h1>
    <p>Umur: {{$cast->umur}}</p>
    <p>Bio: <br> {{$cast->bio}}</p>
@endsection