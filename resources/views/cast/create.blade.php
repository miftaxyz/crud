@extends('layout.master')

@section('judul')
    Tambah Pemain
@endsection

@section('content')
    
<div>
    <h2>Tambah Pemain</h2>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama Pemain Film</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Pemain">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur Pemain">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio</label>
                <textarea type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan Umur Pemain"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection