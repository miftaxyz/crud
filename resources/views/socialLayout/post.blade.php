@extends('socialLayout.master')

@section('content')

<div class="card shadow-sm">
    <div class="row align-items-center px-2 pt-2 pb-2 d-flex justify-content-between">
        <div class="col-1">
            <img src=@yield('foto') alt="profile" width="40px"/>
        </div>
        <div class="col-4">
            <p class="card-title ">@yield('username')</p>
        </div>
        <div class="col-1">
            <li class="dropdown justify-content-end">
                <a href="#" data-toggle="dropdown">
                  <i class="icon-ellipsis mx-0"></i>                  
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list">
                  <p class="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-success">
                        <i class="ti-info-alt mx-0"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <h6 class="preview-subject font-weight-normal">Application Error</h6>
                      <p class="font-weight-light small-text mb-0 text-muted">
                        Just now
                      </p>
                    </div>
                  </a>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-warning">
                        <i class="ti-settings mx-0"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <h6 class="preview-subject font-weight-normal">Settings</h6>
                      <p class="font-weight-light small-text mb-0 text-muted">
                        Private message
                      </p>
                    </div>
                  </a>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-info">
                        <i class="ti-user mx-0"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <h6 class="preview-subject font-weight-normal">New user registration</h6>
                      <p class="font-weight-light small-text mb-0 text-muted">
                        2 days ago
                      </p>
                    </div>
                  </a>
                </div>
              </li>
        </div>
      </div>
    
    <img src=@yield('imagePost') alt="" class="card-img-top">
    <div class="card-body">     
      <a href="" class="btn btn-outline-danger btn-sm"><i class="fa fa-heart" aria-hidden="true"></i></a>
      <p class="card-text">@yield('textPost')</p>

      <div class="input-group mb-3">
        <textarea class="form-control" placeholder="Tambah Komentar" id="floatingTextarea">@yield('komen')</textarea>
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="button">Kirim</button>
        </div>
      </div>
    </div>
   </div>
@endsection