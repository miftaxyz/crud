<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/data-tables', function(){
    return view('table.datatable');
});
Route::get('/table', function(){
    return view('table.table');
});

// Route::get('/cast/create','CastController@create');
// Route::post('/cast','CastController@store');
// Route::get('/cast','CastController@index');
// Route::get('/cast/{cast_id}','CastController@show');
// Route::get('/cast/{cast_id}/edit','CastController@edit');
// Route::put('/cast/{cast_id}','CastController@update');
// Route::delete('/cast/{cast_id}','CastController@destroy');

Route::resource('socialPage','PlayController');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
